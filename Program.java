package example;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import static example.Crawler.ExtremumMode.MIN;

public class Program
{

	public static void main( String[] args ) throws IOException, InterruptedException {
	//List<Student> students = StudentsParser.parse( new URL( "http://home.agh.edu.pl/~ggorecki/IS_Java/students.txt" ) );
	//	List<Student> studentss = StudentsParser.parse( new File( "students.txt" ) );
		//for( Student el : students )
	//		System.out.println( el.getMark() + " " + el.getFirstName() + " " + el.getLastName() + " " + el.getAge() );
		Crawler proba = new Crawler();

		final Logger[] loggers = new Logger[]
				{

						new MailLogger(),
						new ConsolLogger()
				};


		try {
			proba.pobranieUrl();
		}
		catch(NoInputExpection e)
		{
			System.out.println(e.getMessage());
		}

		proba.addIterationStartedListener((i)->{System.out.println("Numer " +i);});
		proba.addToAddStudentsListnerList((student)->{
			String status = proba.isAdd(student);
			for( Logger el : loggers )
				el.log(status, student );

		});
        proba.addToDeleteStudentsListnerList((student)->{
        	String status = proba.isRemove(student);
        	for( Logger el : loggers )
				el.log(status, student );
        });
		proba.addToModifyStudentsListnerList((student)->{
			String status = proba.isModify(student);
			for( Logger el : loggers )
				el.log(status, student );
		});
      proba.Run();
	}

	//(()->)



}
