package example;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static javax.mail.internet.InternetAddress.*;

/**
 * Created by Bartek on 2017-03-18.
 */
public class MailLogger implements Logger {
    // private final String userName;
    //  private final String password;
    // private final String recipient;

    public MailLogger() {
        // this.userName = userName;
        // this.password = password;
        // this.recipient = recipient;
    }

    @Override
    public void log(String status, Student student) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("bartomiejleja@gmail.com", "sim13vetson");
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("bartomiejleja@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("bartlomiejleja@gmail.com"));
            if (status == "ADDED") {
                message.setSubject(status);
                message.setText("ADDED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
                Transport.send(message);
            }

             if(status == "REMOVED") {
                message.setSubject(status);
                message.setText("REMOVED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
                Transport.send(message);
             }
            if(status == "MODIFY") {
               message.setSubject(status);
                message.setText("MODIFIED: " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
                Transport.send(message);
             }


            //System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
